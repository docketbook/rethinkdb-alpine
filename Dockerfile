FROM alpine:latest

USER root

ADD build /home/build

RUN adduser build -D \
	&& echo 'build ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers \
	&& mkdir -p /var/cache/distfiles \
	&& addgroup build abuild \
	&& chgrp abuild /var/cache/distfiles \
	&& chmod 777 /var/cache/distfiles \
	&& chmod -R 777 /home/build \
	&& apk update \ 
	&& apk add sudo

USER build

RUN sudo apk update \
	&& sudo apk add --no-cache --virtual .build-deps \
	g++ bash protobuf-dev protobuf-c python \
	nodejs icu-dev openssl-dev make wget \ 
	curl-dev boost-dev jemalloc-dev linux-headers alpine-sdk \
	bsd-compat-headers m4 paxctl paxmark \
	&& cd /home/build \
	&& abuild-keygen -a -n \
	&& sudo cp /home/build/.abuild/*.pub /etc/apk/keys \
	&& cd libexecinfo  \
	&& abuild checksum && abuild -r \
	&& cd ../ \
	&& sudo apk add packages/build/x86_64/libexecinfo-dev-1.1-r0.apk packages/build/x86_64/libexecinfo-1.1-r0.apk --allow-untrusted \
	&& cd /home/build/rethinkdb \
	&& abuild checksum \
	&& abuild -r \
	&& cd ../ \
	&& sudo apk add packages/build/x86_64/rethinkdb-2.3.0-r0.apk --allow-untrusted \
	&& cd /home/build \
	&& sudo apk del .build-deps \
	&& sudo apk del sudo \
	&& rm -rf /home/build/*

USER root

VOLUME ["/data"]

WORKDIR /data

CMD ["rethinkdb", "--bind", "all"]

EXPOSE 8080 28015 29015